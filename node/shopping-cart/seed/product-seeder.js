var Product= require('../models/product');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('localhost:27017/shopping');

var products = [new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
}),new Product({

    imagePath  : 'http://localhost:3000/images/Jean-Paul-gaultier-ss-16.jpg',
    title  :  "Awesome Watch",
    description  : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos dolorum laborum necessitatibus quidem repellendus? Deserunt dolorem doloremque esse fugiat fugit minus modi perspiciatis recusandae, repellendus repudiandae sequi sint voluptatem, voluptates!",
    price  : 10
})

];

var done=0;
for(var i=0; i< products.length;i++){

    products[i].save(function(err,result){

        done++;
        if(done===products.length){
            exit();
        }
    });
}

function exit(){
    mongoose.disconnect();
}
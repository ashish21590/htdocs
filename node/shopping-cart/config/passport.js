var passport=require('passport');
var User = require('../models/user');
var LocalStrategy= require('passport-local').Strategy;

passport.serializeUser(function(user,done){

    done(null,user.id);

});

passport.deserializeUser(function(id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local_signup', new LocalStrategy({
        // Define what form 'name' fields to use as username and password equivalents:
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback :true
    },
    function(req,email, password, done) {
        process.nextTick(function() {
            req.checkBody('email','invalid email').isEmail();
            req.checkBody('password','invalid password').isLength({min:4});
            var errors = req.validationErrors();
            var messages=[];
            if(errors){
                errors.forEach(function(error){
                    //console.log(errors);
                    messages.push(error.msg);
                });

                console.log("errors are "+messages);
                return done(null,false,req.flash('error',messages));

            }
            // Attempt to find a user with this email:
            User.findOne({
                email: email
            }, function(err, user) {
                if(err) {
                    done(err);
                    return;
                }

                if(user) {
                    done(null, false, { message: 'Email already in use.' });
                    return;
                }

                // User does not exist with this email, so create it:
                var newUser = new User();

                newUser.email = email;
                newUser.password = newUser.enctypePassword(password); // Call instance method 'generateHash' to produce password hash

                // Save this user to our database and return it to Passport by passing it as the second
                // parameter of the 'done' callback:
                newUser.save(function(err) {
                    if(err) {
                        done(err);
                        return;
                    }

                    done(null, newUser);
                });
            })
        });
    }));


// for signin user

passport.use('local_signin',new LocalStrategy({

    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback :true

},function(req,email,password,done){
    req.checkBody('email','invalid email').isEmail();
    req.checkBody('password','invalid password').isLength({min:4});
    var errors = req.validationErrors();
    var messages=[];
    if(errors){
        errors.forEach(function(error){
            //console.log(errors);
            messages.push(error.msg);
        });

        console.log("errors are "+messages);
        return done(null,false,req.flash('error',messages));

    }

    User.findOne({ email:email}, function(err,user){
        console.log("this is the login data ="+user);
        if(err){

            return done(err);
        }
        if(!user){
            return done(null,false,{ message: 'no user found'})
        }
        if(!user.validPassword(password)){
            return done(null,false,{message :'wrong password'})
        }
        return done(null,user);
    })
}));